﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YT
{


    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float speed;
        
        [Space]
        public float maxFallingSpeed = 100f;
        public float initialFallingSpeed = 0f;

        private Vector3 screenPoint;
        private Vector3 offset;

        private Rigidbody rb;
        private void Start()
        {
            rb = GetComponent<Rigidbody>();
            rb.velocity = Vector3.down * initialFallingSpeed;
        }

        private void Update()
        {
            //Rigidbody max speed equalize
            if (rb.velocity.magnitude > maxFallingSpeed)
            {
                rb.velocity = rb.velocity.normalized * maxFallingSpeed;
            }

            //Farenin bastıgı noktayı al.
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);

                offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

            }

            //Fare hareketine göre objeyi hareket ettir.
            if (Input.GetKey(KeyCode.Mouse0))
            {
                Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

                Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
                curPosition = new Vector3(curPosition.x, rb.position.y, curPosition.z);
                rb.position = curPosition;
            }
        }


    }
}