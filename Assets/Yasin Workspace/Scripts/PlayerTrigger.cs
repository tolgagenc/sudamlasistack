﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace YT
{
    public class PlayerTrigger : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Raindrop"))
            {
                Vector3 scale = transform.localScale;
                scale += new Vector3(0.1f, 0.1f, 0.1f);
                transform.localScale = scale;
                Destroy(other.gameObject);
            }
        }
    }
}