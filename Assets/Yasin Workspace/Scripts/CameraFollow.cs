﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YT
{
    public class CameraFollow : MonoBehaviour
    {
        public float speed;
        public Transform target;

        private Vector3 offset;

        private void Start()
        {
            offset = transform.position - target.position;
        }

        private void LateUpdate()
        {
            Vector3 pos = Vector3.Lerp(transform.position, target.position + offset, speed);

            transform.position = new Vector3(transform.position.x, pos.y, transform.position.z);
        }

    }
}
